import java.util.Random;

public class Board{
	private Tile[][] grid;
	private String[][] gridColours;
	private int size;
	
	//Constructor
	Board(int size){
		this.size = size;
		this.grid = new Tile[this.size][this.size];
		
		Random rng = new Random();
		
		for(int i = 0; i < this.size; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				double percent = rng.nextDouble();
				//30% chance of being a wall
				if(percent < 0.30)
					this.grid[i][j] = Tile.HIDDEN_WALL;
				else
					this.grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	public String toString(){		
		String output = "		";
		
		// adds the tens column number
		for(int i = 0; i <= this.size; i++){
			if(i/10 == 0)
				output += "  ";
			else				
				output += i/10 + " ";
		}
		
		output += "\n		";
		
		// adds the units column number
		for(int i = 0; i <= this.size; i++){
			output += i%10 + " ";
		}
		
		output += "\n";
		
		for(int i = 0; i < this.size; i++){
			// i + 1 adds the row number
			if(i + 1 < 10)
				output += "		"+ (i + 1) + " ";
			else
				output += "	       "+ (i + 1) + " ";
			for(int j = 0; j < this.size; j++){
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;		
	}	
	
	//return -2 if it's out of the board, -1 if there is a castle or wall, 1 if there is an hidden wall and 0 if a Castle can be placed
	public int placeToken(int row, int col){
		if(row < 1 || row > this.size || col < 1 || col > this.size){
			return -2;
		} else if (this.grid[row - 1][col - 1] == Tile.CASTLE || this.grid[row - 1][col - 1] == Tile.WALL){
			return -1;
		} else if (this.grid[row - 1][col - 1] == Tile.HIDDEN_WALL){
			this.grid[row - 1][col - 1] = Tile.WALL;
			return 1;
		} else {
			this.grid[row - 1][col - 1] = Tile.CASTLE;
			return 0;
		}		
	}
	
	//Couts number of hidden walls
	public int numberOfHiddenWalls(){
		int numberOfWalls = 0;
		for(int i = 0; i < this.size; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[i][j] == Tile.HIDDEN_WALL)
					numberOfWalls++;
			}
		}
		return numberOfWalls;
	}
	
	public String endGameBoard(){
		// 0 1 2 3 4 5 adds the column number
		String output = "		";
		
		// adds the tens column number
		for(int i = 0; i <= this.size; i++){
			if(i/10 == 0)
				output += "  ";
			else				
				output += i/10 + " ";
		}
		
		output += "\n		";
		
		// adds the units column number
		for(int i = 0; i <= this.size; i++){
			output += i%10 + " ";
		}
		
		output += "\n";
		
		for(int i = 0; i < this.size; i++){
			// i + 1 adds the row number
			if(i + 1 < 10)
				output += "\u001B[37m		"+ (i + 1) + " ";
			else
				output += "\u001B[37m	       "+ (i + 1) + " ";
			for(int j = 0; j < this.size; j++){
				output += "\u001B[30m" + this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;		
	}
}