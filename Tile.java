public enum Tile{
	//White background
	BLANK("\u001B[47m \u001B[40m"),
	//Red W
	WALL("\u001B[31mW\u001B[37m"),
	//White background
	HIDDEN_WALL("\u001B[47mH\u001B[40m"),
	//Green C
	CASTLE("\u001B[32mC\u001B[37m");
	
	private final String name;
	
	private Tile(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}