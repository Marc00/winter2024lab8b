import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);				
		
		System.out.println(" Hello!! Let's play Castle\n");
		System.out.println(" What size board would you like?\n");
		//double because of the odds calculation
		double size = Double.parseDouble(scan.nextLine());				
		
		//(int) converts the double to int
		Board board = new Board((int)size);			
		
		System.out.println("\033[H\033[2J");
		
		int numCastles = ((int)size * 7)/5;
		int turns = ((int)size * 8)/5;
		
		while(numCastles > 0 && turns > 0){
			System.out.println(board);
			System.out.println(" Number of Castles left: " + numCastles);
			System.out.println(" Turn: " + turns);			
			System.out.println(" Number of hidden walls: " +  board.numberOfHiddenWalls());			
			double oddsOfPlacingOnWall = board.numberOfHiddenWalls()/(size*size) * 100;
			System.out.println(" Chances of placing on a wall: " +  oddsOfPlacingOnWall +"%");						
			
			System.out.println(" ------------------------------------------------------");
			
			System.out.println(" What row and column would you like to put your Castle?");
			System.out.print(" Row: ");
			int row = Integer.parseInt(scan.nextLine());
		
			System.out.print(" Column: ");
			int col = Integer.parseInt(scan.nextLine());
			
			int tokenPlaced = board.placeToken(row, col);
			
			while(tokenPlaced < 0){
				System.out.println(" Those inputs are invalid");
				System.out.print(" Row: ");
				row = Integer.parseInt(scan.nextLine());
			
				System.out.print(" Column: ");
				col = Integer.parseInt(scan.nextLine());
				
				tokenPlaced = board.placeToken(row, col);
			}
			
			if(tokenPlaced == 1){
				System.out.println(" \nThere was a wall there!");
				turns--;
			} else if(tokenPlaced == 0){
				System.out.println(" \nYou placed a Castle!!");
				turns--;
				numCastles--;
			}
			
			System.out.println("\033[H\033[2J");
		}
		
		
		
		System.out.println(board.endGameBoard());
		
		if(numCastles == 0){
			System.out.println("\u001B[37m Congratulations! You won!!");
		} else{
			System.out.println("\u001B[37m You lost, better luck next time");
		}
		
    }
}